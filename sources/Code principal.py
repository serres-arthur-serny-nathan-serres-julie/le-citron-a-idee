import tkinter as tk
from functools import partial
from random import randint

fenetre = tk.Tk()
fenetre.configure(bg='#fce8a4', cursor="heart")
fenetre.geometry("1800x600")


Idea=["Trouver une idée",
      "Toucher de l'herbe",
      "Cliquer une nouvelle fois sur le bouton",
      "Jouer à un jeu",
      "Passer du temps avec sa famille",
      "Lire un livre",
      "Trouver quelqu'un avec qui commettre un double sucide (amoureux ou non)",
      "Rejouer l'une de ses scènes favorites (qu'importe le médium, évitez juste de blessez qui ce soit... quoi que... ça peut être drôle (non en vrai, ne faîtes de mal à personne, ni même à vous))",
      "Crier",
      "Frapper un coussin",
      "Pleurer",
      "Lancer une musique et danser (l'équipe de ce programe n'est pas responsable de quelconque malaise ou jugement de la part d'autrui)",
      "Compléter l'url suivant (le restant est cachés dans les réponses du générateur) : https://www.youtube.com/watch?v=",
      "01100100 00100000 01001110 00110001",
      "01010001 00100000 01001110 00110010",
      "01110111 00100000 01001110 00110011",
      "00110100 00100000 01001110 00110100",
      "01110111 00100000 01001110 00110101",
      "00111001 00100000 01001110 00110110",
      "01010111 00100000 01001110 00110111",
      "01100111 00100000 01001110 00111000",
      "01011000 00100000 01001110 00110001 00110000",
      "01100011 00100000 01001110 00110001 00110001",
      "01010001 00100000 01001110 00110001 00110010",
      "Partagez la vidéo secrète de ce logiciel avec vos amis",
      "Jouer à déchifrer des codes secrets",
      "Faire un créateur d'idée quand on s'ennuie",
      "Mets tes basket et pars à la chasse à la licorne",
      "Prends 3 ingrédients au hasard et invente une recette",
      "Fais-toi un automassage des pieds",
      "Epluche une pomme ou une poire ou une orange ou une mandarine en ne faisaint qu'une seule longue épluchure",
      "Cherche un poème (livre ou internet) et apprends le par coeur",
      "Monter sur une chaise, prends un objet comme micro et chante une chanson",
      "Ferme les yeux et danse à fond (dans le silence !)",
      "Trouver un animal pour chaque lettre de l'alphabet",
      "Choisis trois animaux et dessine la chimère",
      "Ecris la table de 17 (et apprends la !)",
      "Entraine-toi à faire tourner un stylo/crayon autour de tes doigts",
      "Fais la liste de tous les légumes que tu connais",
      "Cours sur place pendant 10 minute",
      "Invente une fin alternative au dernier film/livre que tu as vu",
      "Créer un bijoux (ou porte clé) avec un objet mignon que tu n'utilisera jamais sinon",
      "Fais le tri dans ton armoire",
      "Fais plusieurs formes géométriques aléatoires et transformer les en souris !",
      "Fais un carnet de notes des dernières films que tu as vu",
      "Fais toi un gouté sur le thème du dernier livre que tu as lu",
      "Faire un fort avec des coussins et chaises",
      "Regarde les gens passer dans la rue et invente leur une vie",
      "Essaye d'écrire avec ta main non dominante. Si tu es ambidextre, essaye avec tes pieds !",
      "Fais toi des tresses. Si tu es chauve, dessine toi des tresses !",
      "Invente un language codé pour écrire des messages secrets",
      "Fais l'acrostiche du prénom de quelqu'un (gentil ou pas...). Si tu ne sais pas comment faire, voici un lien pour t'aider : https://hugolescargot.journaldesfemmes.fr/activites-enfants/49981-acrostiche-prenom/",
      "Invente une représentation de toi... en fée !",
      "Imagine toi dans un univers médiéval fantastique",
      "Invente un jeu de plateau",
      "Prends un magazine (de préférence, pas ceux de ta mère !) et dessinent par dessus : c'est du artzine !",
      "Invente un magasin et écris une critique dessus (positif ou négatif...)",
      "Prépare toi le meilleur goûter de ta vie (et nettoie la cuisine, sinon ta mère va pas être contente...)",
      "Fais ton meilleur sudoku (ou mots croisés). Rien de mieux que les activités de vieux !",
      "Fais du camping à l'intérieur",
      "Fais tes devoirs... Ca fait trop longtemps que tu appuies, arrête de faire genre tu veux vraiment faire un truc.",
      "Tri les photos de ton téléphones",
      "Fais des brochette de fruits et partage les (ou pas)",
      "Réorganise ta chambre",
      "Regarde des vieux films",
      "Fais un album photos",
      "Essaye de nouvelles recettes",
      "Teste des cocktails (sans alcool) qui pourrait faire peur à un mort",
      "Tente le yoga",
      "Fais un peu de méditation (si c'est trop dur, fait celle pour enfant)",
      "Commence le journaling. Voici un site pour t'aider : https://sensy.me/commencer-le-journaling/",
      "Tri le tas de feuille et de cours qui traines chez toi (oui, celui qui est là depuis perpette les zoulous)",
      "Fais des test de culture générale",
      "Fais des test de daltonisme",
      "Fais des test stupide comme 'quelle couleur es tu' ou 'quel canard es tu'",
      "Apprends une phrase qui te fais sourire dans une langue que tu ne connais pas",
      "Invente une langue qui n'existe pas",
      "Apprends les règles d'un jeu de carte ou de société (encore mieux si c'est d'une autre culture, comme le majong !)"]

def Texte(label):
   Sel=randint(1,len(Idea))-1
   label.config(text=Idea[Sel])
   label.configure(bg='#fce8a4')

titre= tk.Label(fenetre, text="Le citron à idées")
titre.configure(bg="#fce8a4", foreground="#f5ba25", font=('Helvetica', 40, 'bold'))
titre.pack()
Soustitre= tk.Label(fenetre, text="Générateur d'idées quand on s'ennuie !")
Soustitre.configure(bg="#fce8a4", foreground="#f5ba25", font=('Helvetica', 25, 'italic'))
Soustitre.pack()

idée=tk.Label(fenetre, text="--", bg="#fce8a4", foreground="#f5ba25", font=('Helvetica', 15))
idée.pack(side=tk.TOP, padx=0, pady=100)

citron = tk.PhotoImage(file = r"Citron.png", master=fenetre )
boutton = tk.Button(fenetre, text="Presse moi !", image=citron,
         command=partial(Texte, idée))
boutton.pack(side=tk.TOP, padx=650, pady=50)

fenetre.mainloop()